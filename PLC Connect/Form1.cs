﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using OMRON.Compolet.SYSMAC;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Net.NetworkInformation;
using System.Configuration;
using System.IO;

namespace PLC_Connect
{
    public partial class Form1 : Form
    {
        //  --------------------   Connection String Setup
        private static SqlConnection con = new SqlConnection("Data Source=192.168.10.10\\SQLEXPRESS;Initial Catalog=THAIARROW;Persist Security Info=True;User ID=Omron;Password=P@ssw0rd");
        //private static SqlConnection con = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=THAIARROW;Integrated Security=True");
        //private static SqlConnection con = new SqlConnection("data source=DESKTOP-SSVOSN2\\SQLEXPRESS;initial catalog = THAIARROW; user id = sa; password=P@ssw0rd");
        private static SqlCommand cmd;
        private static SqlDataReader reader;

        //  Configuration Settings
        public static string _PLC1IP = ConfigurationSettings.AppSettings["PLC_IP"];
        public static string _PLC1Node = ConfigurationSettings.AppSettings["PLC_Node"];
        public static string _Path = ConfigurationSettings.AppSettings["Path_Log"];
        public static string _LineLocation = ConfigurationSettings.AppSettings["LineLocation"];

        //  PLC Setup
        public static SysmacCJ _PLC1 = new SysmacCJ();

        public short[] _PLC1PeerSetting = { 10, Convert.ToInt16(_PLC1Node), 0 };

        public int _count = 0;
        public bool haveNewJob = false;

        public static long _ReceiveTimeLimit = 3000;
        public static int _DelayLoop = 200;

        public static int _qty = 0;
        public static int _qtyBuffer = 0;
        public static int _UpperBound_SerialBegin = 13;
        public static int _UpperBound_SubString = 6;

        public static bool appRunning;

        public static bool sqlConnectionBusy = false;
        public static bool stateErr = false;
        public static bool _serialEvent = false;
        public static string _kumbanCode = "";

        public static Thread threadTimeShift = new Thread(new ThreadStart(threadShiftTime));
        public static Thread threadMcCheck = new Thread(new ThreadStart(threadMcCheckEvent));

        public static Queue<string> _queueProductionPartNo = new Queue<string>();
        public static Queue<int> _queueQty = new Queue<int>();
        public static Queue<string> _queueLineCode = new Queue<string>();
        public static Queue<DateTime> _queueStartTime = new Queue<DateTime>();

        public static string _LineCode = "";
        public static string _LinePLC = "";
        public static string _KumbanCode, _ProductionPartNo, _ProductionPartNoRead, _modelName, _ProductionPartNoBuffer;
        public static string _partNo, _Shift;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //  First Time Initate
            this.TopMost = true;
            appRunning = false;

            _ProductionPartNoBuffer = "";

            comboBoxPN_LineName.SelectedIndex = 0;
            comboBoxPN_Ship.SelectedIndex = 0;

            //  Update Confirm State
            Update_LineSetting_Confirm(_LineLocation, 0);
        }

        //  Setup Button
        private void buttonPN_LineSetup_Click(object sender, EventArgs e)
        {

            bool noErr = true;
            DialogResult result1 = MessageBox.Show("ขณะนี้คุณต้องการเริ่มการทำงานใน Line " + comboBoxPN_LineName.SelectedItem.ToString() + " และกะ " +
                        comboBoxPN_Ship.SelectedItem.ToString(), "การตั้งค่าการทำงานก่อนเริ่มงาน", MessageBoxButtons.YesNo);
            if (result1 == DialogResult.Yes)
            {
                //  Stamp Log
                CreatAndApeendingLogFile("COM5", "Main Task : RUN");

                _LinePLC = comboBoxPN_LineName.SelectedItem.ToString();
                _LineCode = comboBoxPN_LineName.SelectedItem.ToString();
                _Shift = convertShipSelection_comboBoxLM_Shift();

                //  Update Shift and Confirm State
                Update_LineSetting_Status(_LineCode, _Shift);
                Update_LineSetting_Confirm(_LineLocation, 1);

                appRunning = true;

                try
                {
                    Program_0_SetConenction();
                    Thread.Sleep(100);
                    threadMcCheck.Start();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Thread is error : " + ex.Message, "buttonPN_LineSetup_Click");
                    CreatAndApeendingLogFile("COM5", "ERR : " + ex.Message);
                }

                //  Serial 
                try
                {
                    if (serialPortBegin.IsOpen)
                        serialPortBegin.Close();
                    //  Open Port COM5
                    serialPortBegin.Open();
                    //  Stamp Log Files
                    CreatAndApeendingLogFile("COM5", "Start Job");
                }
                catch (Exception ex)
                {
                    noErr = false;
                    MessageBox.Show("Serial Port is error : " + ex.Message, "buttonPN_LineSetup_Click");
                    CreatAndApeendingLogFile("COM5", "ERR :" + ex.Message);
                }

                if (noErr)
                {
                    labelSetupFinish.Visible = true;
                    MessageBox.Show("การตั้งค่าการทำงานสำเร็จ !!!");
                    this.WindowState = FormWindowState.Minimized;
                }

            }
        }
        private void buttonLM_Done_Click(object sender, EventArgs e)
        {
            DialogResult result1 = MessageBox.Show("คุณต้องการล้างการตั้งค่าก่อนหน้าทั้งหมดใช่หรือไม่", "การตั้งค่าการทำงานก่อนเริ่มงาน", MessageBoxButtons.YesNo);
            if (result1 == DialogResult.Yes)
            {
                _queueProductionPartNo.Clear();
                _queueQty.Clear();
                _queueLineCode.Clear();
                _queueStartTime.Clear();

                _qty = 0;
            }
        }

        //  Serial Port
        private void serialPortBegin_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            try
            {
                //  5Y769340973T026
                string buffer = "";
                buffer += serialPortBegin.ReadExisting();
                CreatAndApeendingLogFile("COM5", buffer.Replace("\r\n", ""));
                buffer = buffer.Replace("\t", "");

                // Test for termination character in buffer
                if ((buffer.Contains("\r\n")) && ((buffer.Contains("Y")) || (buffer.Contains("y"))))
                {
                    //  Find y or Y in buffer
                    char y;
                    string Y = "";
                    if (buffer.Contains("Y"))
                    {
                        Y = "Y";
                        y = 'Y';
                    }
                    else
                    {
                        Y = "y";
                        y = 'y';
                    }

                    //  Run code on data received from serial port
                    string kumbanCode = new string(buffer.Where(c => !char.IsControl(c)).ToArray()).Split(y)[1];
                    _kumbanCode = Y + kumbanCode;

                    //  Active Flag
                    if (_serialEvent == false)
                        _serialEvent = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("serialPortBegin : " + ex.Message, "serialPortBegin_DataReceived");
            }
        }

        //  Thread Portion
        public static void threadShiftTime()
        {
            while (true)
            {
                EndShiftwithLocalTime();
                Thread.Sleep(200);
            }
        }
        public static void threadMcCheckEvent()
        {
            while (true)
            {
                //  Check Serial Thread Check
                //CreatAndApeendingLogFile("COM5", "thradSerialDBConnectEvent");
                thradSerialDBConnectEvent();

                if (PingHost(_PLC1IP))
                {
                    //  Machine Status 
                    //CreatAndApeendingLogFile("COM5", "Program_2_GetErrortimeEvent");
                    Program_2_GetErrortimeEvent();
                    //CreatAndApeendingLogFile("COM5", "Program_3_GetAcktimeEvent");
                    Program_3_GetAcktimeEvent();
                    //Program_4_GetPEtimeEvent();
                    //CreatAndApeendingLogFile("COM5", "Program_5_GetSuccessTimeEvent");
                    Program_5_GetSuccessTimeEvent();
                }

                //  Check End Shift
                EndShiftwithLocalTime();

                Thread.Sleep(100);
            }
        }
        public static void thradSerialDBConnectEvent()
        {
            try
            {
                if (_serialEvent)
                {
                    if ((_kumbanCode.Length >= 12) && (_kumbanCode.Length <= 22))
                    {
                        string[] stack = SearchProductionNoinDB(_kumbanCode);
                        string model = stack[1];

                        _ProductionPartNo = stack[0];
                        _LineCode = stack[2];

                        //  Check Plan
                        string qty = SearchProductionPlan(_ProductionPartNo);
                        if (qty != "None")
                        {

                            //  Set D21 = 1 : Alarm
                            //Fins_WriteMemoryWordInteger(_PLC1, 21, 1);

                            //  CASE 0 : First State
                            if (_queueProductionPartNo.Count == 0)
                            {
                                #region CASE : First state while start Program
                                //  SELECT Production Code in DB
                                if (_ProductionPartNo != "None")
                                {
                                    //  Insert New Actual Production in [ActualProductionData]
                                    DateTime startTime = DateTime.Now;
                                    _qty = int.Parse(qty);
                                    _queueQty.Enqueue(_qty);
                                    _queueLineCode.Enqueue(_LineCode);
                                    _queueProductionPartNo.Enqueue(_ProductionPartNo);
                                    _queueStartTime.Enqueue(startTime);
                                    InsertProductionOKtoDB(_ProductionPartNo, model, _qty, startTime);
                                }
                                else
                                {
                                    string message = "Production Code ไม่พบใน Plan กรุณาตรวจสอบ Plan หรือหมายเลข Kumban ในระบบ";
                                    string caption = "PLC Connection Message";
                                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                                    DialogResult result;

                                    // Displays the MessageBox.
                                    result = MessageBox.Show(message, caption, buttons);
                                }
                                #endregion
                            }

                            //  CASE 1 : NEW KANBAN INCOMING
                            else if ((_ProductionPartNo != _queueProductionPartNo.Peek()) && (_queueProductionPartNo.Count > 0))
                            {
                                #region CASE : Scanner get new work from kanban scanner

                                //  OPEN THE NEW JOB AND CLOSE CURRENT JOB
                                if (_ProductionPartNo != "None")
                                {
                                    //  OPEN NEW JOB
                                    DateTime startTime = DateTime.Now;
                                    _qty = int.Parse(qty);
                                    _queueQty.Enqueue(_qty);
                                    _queueLineCode.Enqueue(_LineCode);
                                    _queueProductionPartNo.Enqueue(_ProductionPartNo);
                                    _queueStartTime.Enqueue(startTime);
                                    InsertProductionOKtoDB(_ProductionPartNo, model, _qty, startTime);

                                    //  CLOSE CURRENT JOB
                                    DateTime finishDateTime = DateTime.Now;
                                    UpdateProductionEndtoDB(_queueProductionPartNo.Peek(), _queueLineCode.Peek(), _queueStartTime.Peek(), startTime, _queueQty.Peek());

                                    //  Load Buffer
                                    _qty = _qtyBuffer;
                                    _queueQty.Dequeue();
                                    _queueLineCode.Dequeue();
                                    _queueStartTime.Dequeue();
                                    _queueProductionPartNo.Dequeue();

                                }
                                else
                                {
                                    string message = "Production Code ไม่พบใน Plan กรุณาตรวจสอบ Plan หรือหมายเลข Kumban ในระบบ";
                                    string caption = "PLC Connection Message";
                                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                                    DialogResult result;

                                    // Displays the MessageBox.
                                    result = MessageBox.Show(message, caption, buttons);
                                }

                                #endregion

                            }
                        }
                        else
                        {
                            //  Set D20 = 2 : Alarm
                            CreatAndApeendingLogFile("COM5", "Alarm no plan");
                            Fins_WriteMemoryWordInteger(_PLC1, 20, 4);
                            string m = "ไม่มี PartNo : " + _ProductionPartNo + " ใน Plan";
                            DialogResult dialogResult = MessageBox.Show(m, "PLC Connect Message", MessageBoxButtons.OK);
                            if (dialogResult == DialogResult.OK)
                            {
                                //clear tower light
                                Fins_WriteMemoryWordInteger(_PLC1, 20, 1);
                                CreatAndApeendingLogFile("COM5", "Ack no plan");
                            }
                            else 
                            {
                                //clear tower light
                                Fins_WriteMemoryWordInteger(_PLC1, 20, 1);
                                CreatAndApeendingLogFile("COM5", "Ack no plan");
                            }
                        }
                    }

                    //  Reset _serialEvent
                    _serialEvent = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "thradSerialDBConnectEvent");
            }
        }

        //  SQL Function for Update Current Data  (Addition 24/06/18)
        public static void Update_LineSetting_Status(string lineCode, string currentShift)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                cmd.CommandText = "UPDATE [LinePlcCon]" +
                    " SET [shift] = @shift" +
                    " WHERE [lineCode] = @lineCode";

                cmd.Parameters.AddWithValue("@shift", currentShift);
                cmd.Parameters.AddWithValue("@lineCode", lineCode);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
                sqlConnectionBusy = false;
            }
        }
        public static void Update_LineSetting_Confirm(string lineCode, int confirmState)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                cmd.CommandText = "UPDATE [LinePlcCon]" +
                    " SET [confirm] = @confirm" +
                    " WHERE [lineCode] = @lineCode";

                cmd.Parameters.AddWithValue("@confirm", confirmState);
                cmd.Parameters.AddWithValue("@lineCode", lineCode);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
                sqlConnectionBusy = false;
            }
        }

        #region Production Portion
        //  SQL Commu. (Production Data) - INSERT + UPDATE
        public static void InsertProductionOKtoDB(string ProductionNo, string model, int Qty, DateTime datetimeStart)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                cmd.CommandText = "INSERT INTO [ActualProductionData]" +
                    "([datetime],[model],[productionNo],[Qty],[Ship],[Status],[lineCode],[startTime])" +
                    " VALUES (@datetime,@model,@productionNo,@Qty,@Ship,@Status,@lineCode,@startTime)";
                cmd.Parameters.AddWithValue("@datetime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                cmd.Parameters.AddWithValue("@model", model);
                cmd.Parameters.AddWithValue("@productionNo", ProductionNo);
                cmd.Parameters.AddWithValue("@Qty", Qty);
                cmd.Parameters.AddWithValue("@Ship", _Shift);
                cmd.Parameters.AddWithValue("@Status", "Working");
                cmd.Parameters.AddWithValue("@lineCode", _LineCode);
                cmd.Parameters.AddWithValue("@startTime", datetimeStart.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                cmd.ExecuteNonQuery();
                //cmd.CommandText = " UPDATE [Planning] SET [WorkState] = '1'" +
                //    " FROM (SELECT TOP 1 * from Planning  WHERE [Yazaki Parts No] = @productionNo AND Qty = @Qty AND lineCode = @lineCode AND [Ship] = @Ship AND CAST(datetime as date) = @date ORDER BY datetime ASC) AS A " +
                //    " WHERE[Planning].[Yazaki Parts No] = A.[Yazaki Parts No] AND [Planning].Qty = A.Qty AND [Planning].lineCode = a.lineCode AND [Planning].Ship = a.Ship AND [Planning].datetime = a.datetime";
                //cmd.Parameters.AddWithValue("@productionNo", ProductionNo);
                //cmd.Parameters.AddWithValue("@Qty", Qty);
                //cmd.Parameters.AddWithValue("@lineCode", _LineCode);
                //cmd.Parameters.AddWithValue("@Ship", _Shift);
                //cmd.Parameters.AddWithValue("@date", DateTime.Now.ToString("yyyy-MM-dd"));

                cmd.CommandText = " UPDATE [Planning] SET [WorkState] = '1'" +
                     " FROM (SELECT TOP(1) * from Planning  WHERE [Yazaki Parts No] = '" + ProductionNo + "' AND Qty = '" + Qty + "' AND lineCode = '" + _LineCode + "'" +
                     " AND [Ship] = '" + _Shift + "' AND CAST(datetime as date) = '" + DateTime.Now.ToString("yyyy-MM-dd") + "' AND (WorkState = 0 or WorkState is null) ORDER BY datetime ASC) AS A" +
                     " WHERE[Planning].[Yazaki Parts No] = A.[Yazaki Parts No]  AND[Planning].Qty = A.Qty AND[Planning].lineCode = a.lineCode AND[Planning].Ship = a.Ship " +
                     " AND[Planning].datetime = a.datetime AND([Planning].WorkState = 0 or[Planning].WorkState is null)";
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "InsertProductionOKtoDB");
            }
            finally
            {
                con.Close();
                sqlConnectionBusy = false;
            }
        }
        public void UpdateProductionOKtoDB(string productionNo, string lineCode, int value, DateTime startTime)
        {
            //  Read Machine 6 cases
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                string month = DateTime.Now.Month.ToString();
                cmd.CommandText = "UPDATE [ActualProductionData]" +
                    " SET [datetime] = @datetime," +
                    " [OK] = '" + value + "'," +
                    " [Status] = 'Working'" +
                    " WHERE [productionNo] = @productionNo" +
                    " AND [lineCode] = @lineCode" +
                    " AND YEAR([datetime]) = '" + DateTime.Now.Year + "'" +
                    " AND MONTH([datetime]) = '" + DateTime.Now.Month + "'" +
                    " AND DAY([datetime]) = '" + DateTime.Now.Day + "'" +
                    " AND [startTime] = @startTime";
                cmd.Parameters.AddWithValue("@datetime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                cmd.Parameters.AddWithValue("@productionNo", productionNo);
                cmd.Parameters.AddWithValue("@lineCode", lineCode);
                cmd.Parameters.AddWithValue("@startTime", startTime.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "UpdateProductionOKtoDB");
            }
            finally
            {
                con.Close();
                sqlConnectionBusy = false;
            }
        }
        public static void UpdateProductionEndtoDB(string productionNo, string lineCode, DateTime startTime, DateTime datetimeFinish, int value)
        {
            //  Read Machine 6 cases
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                string month = DateTime.Now.Month.ToString();
                cmd.CommandText = "UPDATE [ActualProductionData]" +
                    " SET [datetime] = @datetime," +
                    " [OK] = '" + value + "'," +
                    " [finishTime] = @finishTime," +
                    " [Status] = 'Suscess'" +
                    " WHERE [productionNo] = @productionNo" +
                    " AND [lineCode] = @lineCode" +
                    " AND YEAR([datetime]) = '" + DateTime.Now.Year + "'" +
                    " AND MONTH([datetime]) = '" + DateTime.Now.Month + "'" +
                    " AND DAY([datetime]) = '" + DateTime.Now.Day + "'" +
                    " AND [startTime] = @startTime";
                cmd.Parameters.AddWithValue("@datetime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                cmd.Parameters.AddWithValue("@productionNo", productionNo);
                cmd.Parameters.AddWithValue("@lineCode", lineCode);
                cmd.Parameters.AddWithValue("@finishTime", datetimeFinish.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                cmd.Parameters.AddWithValue("@startTime", startTime.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "UpdateProductionEndtoDB");
            }
            finally
            {
                con.Close();
                sqlConnectionBusy = false;
            }
        }

        //  SQL Commu. (Production Data) - SELECT
        public static string[] SearchProductionNoinDB(string kumbanCodeIn)
        {
            string[] result = new string[3];
            string kumbanCode = "";

            kumbanCode = kumbanCodeIn.Replace("Y", "").Replace("y", "").Replace("J", "").Replace("j", "").Substring(0, _UpperBound_SubString);

            //  Check is Auto Line
            if ((_LinePLC == "K-7901") || (_LinePLC == "K-7902") || (_LinePLC == "K-7903") || (_LinePLC == "K-7904"))
            {
                kumbanCode = kumbanCodeIn;
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                string month = DateTime.Now.Month.ToString();
                cmd.CommandText = "SELECT [partNo] ,[modelName],[lineCode] FROM [MasterProduct]" +
                    " WHERE [kumbanCode] like '%" + kumbanCode + "%'";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result[0] = reader.GetValue(0).ToString();
                        result[1] = reader.GetValue(1).ToString();
                        result[2] = reader.GetValue(2).ToString();
                    }
                }
                else
                {
                    result[0] = "None";
                    result[1] = "None";
                    result[2] = "None";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "SearchProductionNoinDB");
            }
            finally
            {
                reader.Close();
                cmd.Dispose();
                con.Close();
                sqlConnectionBusy = false;
            }
            return result;
        }
        public static string SearchProductionPlan(string ProductionNo)
        {
            string result = "";

            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                string month = DateTime.Now.Month.ToString();
                cmd.CommandText = "SELECT TOP(1) [Qty] FROM [Planning]" +
                    " WHERE [Yazaki Parts No] = '" + ProductionNo + "'" +
                    " AND [Ship] = '" + _Shift + "'" +
                    " AND YEAR([Day Plan]) = '" + DateTime.Now.Year.ToString() + "'" +
                    " AND MONTH([Day Plan]) = '" + DateTime.Now.Month.ToString() + "'" +
                    " AND DAY([Day Plan]) = '" + DateTime.Now.Day.ToString() + "'" +
                    " AND ([WorkState] = '0' OR [WorkState] IS NULL)" +
                    " ORDER BY no ASC";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result = reader.GetValue(0).ToString();
                    }
                }
                else
                {
                    result = "None";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "SearchProductionPlan");
            }
            finally
            {
                reader.Close();
                cmd.Dispose();
                con.Close();
                sqlConnectionBusy = false;
            }
            return result;
        }
        #endregion

        #region MC Loss Portion
        //  SQL Commu. (MC Loss Data)   -   INSERT
        public static void InsertErrortimeEventtoDB(DateTime errorTime, string lineCode)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                cmd.CommandText = "INSERT INTO [ActualMachineLoss]" +
                    "([datetime],[errorTime],[lineCode],[transState],[ship])" +
                    " VALUES (@datetime,@datetime,@lineCode,@transState,@ship)";
                cmd.Parameters.AddWithValue("@datetime", errorTime);
                cmd.Parameters.AddWithValue("@lineCode", lineCode);
                cmd.Parameters.AddWithValue("@transState", 2);
                cmd.Parameters.AddWithValue("@ship", _Shift);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
                sqlConnectionBusy = false;
            }
        }
        public static void InsertActualMachineLoss(string[] ackData, string[] peData, DateTime finTime)
        {
            string timeDiff = finTime.Subtract(Convert.ToDateTime(ackData[1])).TotalMinutes.ToString("0.##");
            string dateTimeError = Convert.ToDateTime(ackData[1]).ToString("yyMMddHHmmss");

            int n;
            string errorCode = "";
            //var isNumeric = ackData[2], out n);

            //if (isNumeric)
            //    errorCode = n.ToString("D3");
            //else
            //    errorCode = ackData[2];

            errorCode = ackData[2];

            string[] supName = ChangeSupIDtoName(ackData);
            string[] peName = ChangePeIDtoName(peData);
            int supCnt = CountingSup(ackData);
            int peCnt = CountingPe(peData);

            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                cmd.CommandText = "INSERT INTO [ActualMachineLoss]" +
                    "([id],[machineName],[descriptionType],[datetime],[ship],[errorTime],[ackTime],[readyTime],[totalTime]" +
                        ",[supCnt],[peCnt],[problem],[solve],[sup1],[sup2],[sup3],[sup4],[sup5]" +
                        ",[pe1],[pe2],[pe3],[pe4],[pe5],[lineCode])" +
                    " VALUES (@id,@machineName,@descriptionType,@datetime,@ship,@errorTime,@ackTime,@readyTime,@totalTime" +
                        ",@supCnt,@peCnt,@problem,@solve,@sup1,@sup2,@sup3,@sup4,@sup5" +
                        ",@pe1,@pe2,@pe3,@pe4,@pe5,@lineCode)";

                cmd.Parameters.AddWithValue("@id", 3);
                cmd.Parameters.AddWithValue("@machineName", ackData[0]);
                cmd.Parameters.AddWithValue("@descriptionType", errorCode);
                cmd.Parameters.AddWithValue("@datetime", Convert.ToDateTime(ackData[1]));
                cmd.Parameters.AddWithValue("@ship", _Shift);
                cmd.Parameters.AddWithValue("@errorTime", Convert.ToDateTime(ackData[1]));
                cmd.Parameters.AddWithValue("@ackTime", Convert.ToDateTime(ackData[3]));
                cmd.Parameters.AddWithValue("@readyTime", finTime);
                cmd.Parameters.AddWithValue("@totalTime", timeDiff);
                cmd.Parameters.AddWithValue("@supCnt", supCnt);
                cmd.Parameters.AddWithValue("@peCnt", peCnt);
                cmd.Parameters.AddWithValue("@problem", "");
                cmd.Parameters.AddWithValue("@solve", "");
                cmd.Parameters.AddWithValue("@sup1", ackData[4]);
                cmd.Parameters.AddWithValue("@sup2", ackData[5]);
                cmd.Parameters.AddWithValue("@sup3", ackData[6]);
                cmd.Parameters.AddWithValue("@sup4", ackData[7]);
                cmd.Parameters.AddWithValue("@sup5", ackData[8]);
                cmd.Parameters.AddWithValue("@pe1", peData[0]);
                cmd.Parameters.AddWithValue("@pe2", peData[1]);
                cmd.Parameters.AddWithValue("@pe3", peData[2]);
                cmd.Parameters.AddWithValue("@pe4", peData[3]);
                cmd.Parameters.AddWithValue("@pe5", peData[4]);
                cmd.Parameters.AddWithValue("@lineCode", _LinePLC);
                cmd.ExecuteNonQuery();
                cmd.CommandText = "UPDATE THAIARROW.dbo.ActualMachineLoss " +
                                    "SET machineName = (SELECT [mcName] FROM [THAIARROW].[dbo].[MasterMachineLoss] " +
                                    "WHERE mcCode = '" + ackData[0] + "')" +
                                    "WHERE machineName = '" + ackData[0] + "'";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "UPDATE THAIARROW.dbo.ActualMachineLoss " +
                                    "SET problem = (SELECT [mcErrorTopic] FROM [THAIARROW].[dbo].[MasterMachineTopic] " +
                                    "WHERE mcErrorCode = '" + errorCode + "')" +
                                    "WHERE descriptionType = '" + errorCode + "'";
                cmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "InsertActualMachineLoss");
            }
            finally
            {
                con.Close();
                sqlConnectionBusy = false;
            }
        }

        //  SQL Commu. (MC Loss Data)   -   CHECK
        public static bool CheckAckTimeEventHappened()
        {
            //CreatAndApeendingLogFile("COM5", "CheckAckTimeEventHappened 1");
            //  Check State = 1
            bool result = false;
            string mcCode = "";
            DateTime errorTime = new DateTime();

            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                    //CreatAndApeendingLogFile("COM5", "CheckAckTimeEventHappened" + sqlConnectionBusy.ToString());
                }
                sqlConnectionBusy = true;
                con.Open();
                string month = DateTime.Now.Month.ToString();
                cmd.CommandText = "SELECT [machineName],[errorTime] FROM [ActualMachineLoss]" +
                    " WHERE [lineCode] = '" + _LinePLC + "'" +
                    " AND [transState] = '3'";

                //CreatAndApeendingLogFile("COM5", "CheckAckTimeEventHappened" + cmd.CommandText);
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    result = true;
                }
            }
            finally
            {
                reader.Close();
                cmd.Dispose();
                con.Close();
                sqlConnectionBusy = false;
            }

            //if (mcCode != "")
            //    UpdateAcktimeStateFromPDA(mcCode, errorTime);

            return result;
        }
        public static bool CheckPETimeEventHappened()
        {
            //  Check State = 1
            bool result = false;
            string mcCode = "";
            DateTime errorTime = new DateTime();

            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                string month = DateTime.Now.Month.ToString();
                cmd.CommandText = "SELECT TOP 1 [mcCode],[errorTime] FROM [TempMC_PE]" +
                    " WHERE [lineCode] = '" + _LinePLC + "'" +
                    " AND [state] = 1";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        mcCode = reader.GetValue(0).ToString();
                        errorTime = reader.GetDateTime(1);
                        result = true;
                    }
                }
            }
            finally
            {
                reader.Close();
                cmd.Dispose();
                con.Close();
                sqlConnectionBusy = false;
            }

            if (mcCode != "") ;
            //UpdatePEtimeStateFromPDA(mcCode, errorTime);

            return result;
        }
        public static bool CheckFinTimeEventHappened()
        {
            //  Check State = 1
            bool result = false;
            string mcCode = "";
            DateTime errorTime = new DateTime();
            DateTime finishTime = new DateTime();
            string transState = "";

            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                string month = DateTime.Now.Month.ToString();
                cmd.CommandText = "SELECT TOP 1 [machineName],[errorTime],[readyTime],[transState] FROM [ActualMachineLoss]" +
                    " WHERE [lineCode] = '" + _LinePLC + "'" +
                    " order by [datetime] desc";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        transState = reader.GetValue(3).ToString();
                        if(transState == "1")
                        result = true;
                    }
                }
            }
            finally
            {
                reader.Close();
                cmd.Dispose();
                con.Close();
                sqlConnectionBusy = false;
            }

            if (mcCode != "")
            {

            }

            return result;
        }

        //  SQL Commu. (MC Loss Data)   -   UPDATE
        public static void UpdateAcktimeStateFromPDA(string mcCode, DateTime errorTime)
        {
            //  Read Machine 6 cases
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                cmd.CommandText = "UPDATE [TempMC_AckTime]" +
                    " SET [state] = 2" +
                    " WHERE [lineCode] = '" + _LinePLC + "'" +
                    " AND [mcCode] = @mcCode" +
                    " AND [errorTime] = @errorTime";
                cmd.Parameters.AddWithValue("@mcCode", mcCode.Replace(" ", ""));
                cmd.Parameters.AddWithValue("@errorTime", errorTime);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
                sqlConnectionBusy = false;
            }
        }
        public static void UpdatePEtimeStateFromPDA(string mcCode, DateTime errorTime)
        {
            //  Read Machine 6 cases
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                cmd.CommandText = "UPDATE [TempMC_PE]" +
                    " SET [state] = 2" +
                    " WHERE [lineCode] = '" + _LinePLC + "'" +
                    " AND [mcCode] = @mcCode" +
                    " AND [errorTime] = @errorTime";
                cmd.Parameters.AddWithValue("@mcCode", mcCode.Replace(" ", ""));
                cmd.Parameters.AddWithValue("@errorTime", errorTime);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
                sqlConnectionBusy = false;
            }
        }

        //  SQL Commu. (MC Loss Data)   -   SELECT
        public static string[] SelectkAcktimeEventFromPDA()
        {
            string[] result = new string[9];
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                string month = DateTime.Now.Month.ToString();
                cmd.CommandText = "SELECT TOP 1 * FROM [TempMC_AckTime]" +
                    " WHERE [lineCode] = '" + _LinePLC + "'" +
                    " AND [state] = 2";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result[0] = reader.GetValue(0).ToString().Replace(" ", "");          //  MC Code
                        result[1] = reader.GetDateTime(2).ToString();       //  Err Time
                        result[2] = reader.GetValue(3).ToString();          //  Err Code
                        result[3] = reader.GetDateTime(4).ToString();       //  ACK Time
                        result[4] = reader.GetValue(5).ToString();          //  Sup 1
                        result[5] = reader.GetValue(6).ToString();          //  Sup 2
                        result[6] = reader.GetValue(7).ToString();          //  Sup 3
                        result[7] = reader.GetValue(8).ToString();          //  Sup 4
                        result[8] = reader.GetValue(9).ToString();          //  Sup 5
                    }
                }
            }
            finally
            {
                reader.Close();
                cmd.Dispose();
                con.Close();
                sqlConnectionBusy = false;
            }
            return result;
        }
        public static string[] SelectkPetimeEventFromPDA()
        {
            string[] result = new string[5];
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                string month = DateTime.Now.Month.ToString();
                cmd.CommandText = "SELECT TOP 1 * FROM [TempMC_PE]" +
                    " WHERE [lineCode] = '" + _LinePLC + "'" +
                    " AND [state] = 2";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result[0] = reader.GetValue(3).ToString();          //  Sup 1
                        result[1] = reader.GetValue(4).ToString();          //  Sup 2
                        result[2] = reader.GetValue(5).ToString();          //  Sup 3
                        result[3] = reader.GetValue(6).ToString();          //  Sup 4
                        result[4] = reader.GetValue(7).ToString();          //  Sup 5
                    }
                }
            }
            finally
            {
                reader.Close();
                cmd.Dispose();
                con.Close();
                sqlConnectionBusy = false;
            }
            return result;
        }
        public static DateTime SelectkFintimeEventFromPDA()
        {
            DateTime result = new DateTime();
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                string month = DateTime.Now.Month.ToString();
                cmd.CommandText = "SELECT TOP 1 [finishTime] FROM [TempMC_FinishTime]" +
                    " WHERE [lineCode] = '" + _LinePLC + "'" +
                    " AND [state] = 1";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result = reader.GetDateTime(0);
                    }
                }
            }
            finally
            {
                reader.Close();
                cmd.Dispose();
                con.Close();
                sqlConnectionBusy = false;
            }
            return result;
        }

        //  SQL Commu. (MC Loss Data)   -   DELETE
        public static void DeleteTempMC_ErrorTime(string lineCode, DateTime errorTime)
        {
            //  Read Machine 6 cases
            cmd = new SqlCommand();
            cmd.Connection = con;

            DateTime errorTime2 = new DateTime();
            errorTime2 = errorTime.AddMilliseconds(997);

            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                cmd.CommandText = "DELETE FROM [TempMC_ErrorTime]" +
                    " WHERE [lineCode] = '" + lineCode + "'" +
                    " AND ([errorTime] >= @errorTime" +
                    " AND [errorTime] <= @errorTime2)";
                cmd.Parameters.AddWithValue("@errorTime", errorTime);
                cmd.Parameters.AddWithValue("@errorTime2", errorTime2);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
                sqlConnectionBusy = false;
            }
        }
        public static void DeleteTempMC_AckTime(string lineCode, string mcCode, DateTime errorTime)
        {
            //  Read Machine 6 cases
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                cmd.CommandText = "DELETE FROM [TempMC_AckTime]" +
                    " WHERE [lineCode] = '" + lineCode + "'" +
                    " AND [mcCode] = @mcCode" +
                    " AND [errorTime] = @errorTime";
                cmd.Parameters.AddWithValue("@mcCode", mcCode);
                cmd.Parameters.AddWithValue("@errorTime", Convert.ToDateTime(errorTime));
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
                sqlConnectionBusy = false;
            }
        }
        public static void DeleteTempMC_PE(string lineCode, string mcCode, DateTime errorTime)
        {
            //  Read Machine 6 cases
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                cmd.CommandText = "DELETE FROM [TempMC_PE]" +
                    " WHERE [lineCode] = '" + lineCode + "'" +
                    " AND [mcCode] = @mcCode" +
                    " AND [errorTime] = @errorTime";
                cmd.Parameters.AddWithValue("@mcCode", mcCode);
                cmd.Parameters.AddWithValue("@errorTime", Convert.ToDateTime(errorTime));
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
                sqlConnectionBusy = false;
            }
        }
        public static void DeleteTempMC_FinishTime(string lineCode, string mcCode, DateTime errorTime)
        {
            //  Read Machine 6 cases
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                cmd.CommandText = "DELETE FROM [TempMC_FinishTime]" +
                    " WHERE [lineCode] = '" + lineCode + "'" +
                    " AND [mcCode] = @mcCode" +
                    " AND [errorTime] = @errorTime";
                cmd.Parameters.AddWithValue("@mcCode", mcCode);
                cmd.Parameters.AddWithValue("@errorTime", Convert.ToDateTime(errorTime));
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
                sqlConnectionBusy = false;
            }
        }
        #endregion

        #region NG Loss Portion
        //  SQL Commu (NG Case)
        public static bool CheckAcktimeNGEventHappened()
        {
            //  Check State = 1
            bool result = false;
            string snNumber = "";
            DateTime errorTime = new DateTime();

            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                string month = DateTime.Now.Month.ToString();
                cmd.CommandText = "SELECT TOP 1 [snNumber],[startNgTime]" +
                    " FROM [TempNG_AckTime]" +
                    " WHERE [ngLocation] = '" + _LinePLC + "'" +
                    " AND [state] = 1";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        snNumber = reader.GetValue(0).ToString();
                        errorTime = reader.GetDateTime(1);
                        result = true;
                    }
                }
            }
            finally
            {
                reader.Close();
                cmd.Dispose();
                con.Close();
                sqlConnectionBusy = false;
            }

            if (snNumber != "")
                UpdateErrtimeStateFromPLC(errorTime);

            return result;
        }
        public static void CheckRepairtimeNGEventHappened()
        {
            string snNumber = "";
            string repairPerson = "";
            DateTime reworkDate = new DateTime();
            DateTime finishRepairTime = new DateTime();
            string status = "";
            string problem = "";
            string solve = "";

            string ngLocation = "";
            string process = "";
            string ngCase = "";
            string kanbanNo = "";
            string checkingPerson = "";
            DateTime startNgTime = new DateTime();

            cmd = new SqlCommand();
            cmd.Connection = con;

            #region Read [TempNG_RepairTime]
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;

                con.Open();
                string month = DateTime.Now.Month.ToString();
                cmd.CommandText = "SELECT TOP 1 [snNumber],[reworkDate],[repairPerson],[finishRepairTime],[status],[problem],[solve]" +
                    " FROM [TempNG_RepairTime]" +
                    " WHERE [state] = 1";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        snNumber = reader.GetValue(0).ToString();
                        reworkDate = reader.GetDateTime(1);
                        repairPerson = reader.GetValue(2).ToString();
                        finishRepairTime = reader.GetDateTime(3);
                        status = reader.GetValue(4).ToString();
                        problem = reader.GetValue(5).ToString();
                        solve = reader.GetValue(6).ToString();
                    }
                }
            }
            finally
            {
                reader.Close();
                cmd.Dispose();
                con.Close();
                sqlConnectionBusy = false;
            }
            #endregion
            #region Read [TempNG_AckTime]
            //  If can Read Data
            if (snNumber != "")
            {
                try
                {
                    while (sqlConnectionBusy)
                    {
                        Thread.Sleep(10);
                    }
                    sqlConnectionBusy = true;
                    con.Open();
                    string month = DateTime.Now.Month.ToString();
                    cmd.CommandText = "SELECT TOP 1 [ngLocation],[process],[ngCase],[kanbanNo],[checkingPerson],[startNgTime]" +
                        " FROM [TempNG_AckTime]" +
                        " WHERE [snNumber] = '" + snNumber + "'" +
                        " AND [state] = 1";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            ngLocation = reader.GetValue(0).ToString();
                            process = reader.GetValue(1).ToString();
                            ngCase = reader.GetValue(2).ToString();
                            kanbanNo = reader.GetValue(3).ToString();
                            checkingPerson = reader.GetValue(4).ToString();
                            startNgTime = reader.GetDateTime(5);
                        }
                    }
                }
                finally
                {
                    reader.Close();
                    cmd.Dispose();
                    con.Close();
                    sqlConnectionBusy = false;
                }

                //  Take data to [ActualPartNG]
                try
                {
                    //    [id],[snNumber],[ngLocation],[process],[ngCase],[partScrap],[repairPerson],[checkingPerson],[reworkDate]
                    //    ,[startNgTime],[startRepairTime],[finishRepairTime],[totalRepairTime],[status],[problem],[solve]
                    while (sqlConnectionBusy)
                    {
                        Thread.Sleep(10);
                    }
                    sqlConnectionBusy = true;
                    con.Open();
                    cmd.CommandText = "INSERT INTO [ActualPartNG]" +
                        "([snNumber],[ngLocation],[process],[ngCase],[partScrap],[repairPerson],[checkingPerson],[reworkDate]," +
                        "[startNgTime],[startRepairTime],[finishRepairTime],[totalRepairTime],[status],[problem],[solve])" +
                        " VALUES (@snNumber,@ngLocation,@process,@ngCase,@partScrap,@repairPerson,@checkingPerson,@reworkDate," +
                        "@startNgTime,@startRepairTime,@finishRepairTime,@totalRepairTime,@status,@problem,@solve)";

                    cmd.Parameters.AddWithValue("@snNumber", snNumber);
                    cmd.Parameters.AddWithValue("@ngLocation", ngLocation);
                    cmd.Parameters.AddWithValue("@process", process);
                    cmd.Parameters.AddWithValue("@ngCase", ngCase);
                    cmd.Parameters.AddWithValue("@partScrap", kanbanNo);
                    cmd.Parameters.AddWithValue("@repairPerson", repairPerson);
                    cmd.Parameters.AddWithValue("@checkingPerson", checkingPerson);
                    cmd.Parameters.AddWithValue("@reworkDate", reworkDate);
                    cmd.Parameters.AddWithValue("@startNgTime", startNgTime);
                    cmd.Parameters.AddWithValue("@startRepairTime", finishRepairTime);
                    cmd.Parameters.AddWithValue("@finishRepairTime", finishRepairTime);
                    cmd.Parameters.AddWithValue("@totalRepairTime", finishRepairTime.Subtract(startNgTime).TotalMinutes.ToString("0.##"));
                    cmd.Parameters.AddWithValue("@status", status);
                    cmd.Parameters.AddWithValue("@problem", problem);
                    cmd.Parameters.AddWithValue("@solve", solve);
                    cmd.ExecuteNonQuery();
                }
                finally
                {
                    con.Close();
                    sqlConnectionBusy = false;
                }

                // Delete Temp DB
                DeleteTempDBInNGCase(snNumber, startNgTime);
            }
            #endregion
        }
        public static void UpdateErrtimeStateFromPLC(DateTime errorTime)
        {
            //  Read Machine 6 cases
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                cmd.CommandText = "UPDATE [TempMC_ErrorTime]" +
                    " SET [state] = 2" +
                    " WHERE [lineCode] = '" + _LinePLC + "'" +
                    " AND [errorTime] >= @errorTime" +
                    " AND [errorTime] <= @errorTime2";
                cmd.Parameters.AddWithValue("@errorTime", errorTime);
                cmd.Parameters.AddWithValue("@errorTime2", errorTime.AddSeconds(+1));
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
                sqlConnectionBusy = false;
            }
        }
        public static void DeleteTempDBInNGCase(string snNumber, DateTime startNgTime)
        {
            //  Read Machine 6 cases
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                cmd.CommandText = "DELETE FROM [TempNG_AckTime]" +
                    " WHERE [snNumber] = '" + snNumber + "'";
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
                sqlConnectionBusy = false;
            }

            //  Read Machine 6 cases
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                cmd.CommandText = "DELETE FROM [TempNG_RepairTime]" +
                    " WHERE [snNumber] = '" + snNumber + "'";
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
                sqlConnectionBusy = false;
            }

            //  Read Machine 6 cases
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                con.Open();
                cmd.CommandText = "DELETE FROM [TempMC_ErrorTime]" +
                    " WHERE [state] = '" + 2 + "'" +
                    " AND [errorTime] >= @startNgTime" +
                    " AND [errorTime] <= @startNgTime2";
                cmd.Parameters.AddWithValue("@startNgTime", startNgTime);
                cmd.Parameters.AddWithValue("@startNgTime2", startNgTime.AddSeconds(+1));
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
                sqlConnectionBusy = false;
            }
        }
        #endregion

        #region Other Function Portion
        private void LineMonitoringLinCodeDropdownLoad()
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            try
            {
                while (sqlConnectionBusy)
                {
                    Thread.Sleep(10);
                }
                sqlConnectionBusy = true;
                comboBoxPN_LineName.Items.Clear();
                con.Close();
                con.Open();
                cmd.CommandText = " SELECT DISTINCT [lineCode] FROM [MasterProduct] WHERE [lineName] = 'Combi P.c.b (SMT)' ORDER BY [lineCode] ASC";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string sName = reader.GetValue(0).ToString();
                        comboBoxPN_LineName.Items.Add(sName);
                    }
                }
            }
            finally
            {
                reader.Close();
                con.Close();
                sqlConnectionBusy = false;
                comboBoxPN_LineName.SelectedIndex = 0;
            }
        }
        private string convertShipSelection_comboBoxLM_Shift()
        {
            string result = "";

            if (comboBoxPN_Ship.SelectedIndex == 0)
            {
                result = "A";
            }
            else if (comboBoxPN_Ship.SelectedIndex == 1)
            {
                result = "B";
            }
            else
            {
                result = "C";
            }
            return result;
        }

        //  Delay Task
        public static void wait(int interval)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            while (sw.ElapsedMilliseconds < interval)
            {
                Application.DoEvents();
            }

            sw.Stop();
        }

        //  Other Function
        public static void EndShiftwithLocalTime()
        {
            DateTime dateTimeCheck = DateTime.Now;
            DateTime dateA = new DateTime(dateTimeCheck.Year, dateTimeCheck.Month, dateTimeCheck.Day, 7, 0, 0);
            DateTime dateB = new DateTime(dateTimeCheck.Year, dateTimeCheck.Month, dateTimeCheck.Day, 15, 30, 0);
            DateTime dateC = new DateTime(dateTimeCheck.Year, dateTimeCheck.Month, dateTimeCheck.Day, 0, 0, 0);

            if (appRunning)
            {
                if ((dateTimeCheck >= dateA) && (dateTimeCheck < dateA.AddSeconds(10)))
                {
                    //  CLOSE CURRENT JOB
                    DateTime finishDateTime = dateTimeCheck;
                    UpdateProductionEndtoDB(_queueProductionPartNo.Peek(), _queueLineCode.Peek(), _queueStartTime.Peek(), finishDateTime, _queueQty.Peek());
                    Application.Exit();
                }
                else if ((dateTimeCheck >= dateB) && (dateTimeCheck < dateB.AddSeconds(10)))
                {
                    //  CLOSE CURRENT JOB
                    DateTime finishDateTime = dateTimeCheck;
                    UpdateProductionEndtoDB(_queueProductionPartNo.Peek(), _queueLineCode.Peek(), _queueStartTime.Peek(), finishDateTime, _queueQty.Peek());
                    Application.Exit();
                }
                else if ((dateTimeCheck >= dateC) && (dateTimeCheck < dateC.AddSeconds(10)))
                {
                    //  CLOSE CURRENT JOB
                    DateTime finishDateTime = dateTimeCheck;
                    UpdateProductionEndtoDB(_queueProductionPartNo.Peek(), _queueLineCode.Peek(), _queueStartTime.Peek(), finishDateTime, _queueQty.Peek());
                    Application.Exit();
                }
            }
        }
        public static string CheckShiftwithLocalTime()
        {
            string result = "";

            DateTime dateTimeCheck = DateTime.Now;
            DateTime dateA = new DateTime(dateTimeCheck.Year, dateTimeCheck.Month, dateTimeCheck.Day, 7, 0, 0);
            DateTime dateB = new DateTime(dateTimeCheck.Year, dateTimeCheck.Month, dateTimeCheck.Day, 15, 30, 0);
            DateTime dateC = new DateTime(dateTimeCheck.Year, dateTimeCheck.Month, dateTimeCheck.Day, 0, 0, 0);

            if ((dateTimeCheck >= dateA) || (dateTimeCheck < dateB))
            {
                result = "A";
            }
            else if (dateTimeCheck >= dateB)
            {
                result = "B";
            }
            else if ((dateTimeCheck >= dateC) || (dateTimeCheck < dateA))
            {
                result = "C";
            }
            return result;
        }
        public static string ASCII2String(string asciiCode)
        {
            string result = "";
            string st = asciiCode;

            for (int i = 0; i < st.Length; i += 2)
            {
                string hexChar = st.Substring(i, 2);
                result = result + (char)Convert.ToByte(hexChar, 16);
            }

            return result;
        }
        public static string[] ChangeSupIDtoName(string[] ackData)
        {
            string[] result = new string[5];

            for (int i = 0; i < 5; i++)
            {
                if (ackData[i + 4] != "")
                {
                    cmd = new SqlCommand();
                    cmd.Connection = con;
                    try
                    {
                        while (sqlConnectionBusy)
                        {
                            Thread.Sleep(10);
                        }
                        sqlConnectionBusy = true;
                        con.Open();
                        cmd.CommandText = "SELECT [name],[surename] FROM [LoginPassword]" +
                            " WHERE [workerID] = '" + ackData[i + 4] + "'";
                        reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            reader.Read();
                            result[i] = reader.GetValue(0).ToString();
                            result[i] = result[i] + " " + reader.GetValue(1).ToString();
                        }
                        else
                        {
                            result[i] = "NO_DB";
                        }
                    }
                    finally
                    {
                        reader.Close();
                        cmd.Dispose();
                        con.Close();
                        sqlConnectionBusy = false;
                    }
                }
                else
                {
                    result[i] = "";
                }
            }

            return result;
        }
        public static string[] ChangePeIDtoName(string[] peData)
        {
            string[] result = new string[5];

            for (int i = 0; i < 5; i++)
            {
                if (peData[i] != "")
                {
                    cmd = new SqlCommand();
                    cmd.Connection = con;
                    try
                    {
                        while (sqlConnectionBusy)
                        {
                            Thread.Sleep(10);
                        }
                        sqlConnectionBusy = true;
                        con.Open();
                        cmd.CommandText = "SELECT [name],[surename]  FROM [LoginPassword]" +
                            " WHERE [workerID] = '" + peData[i] + "'";
                        reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            reader.Read();
                            result[i] = reader.GetValue(0).ToString();
                            result[i] = result[i] + " " + reader.GetValue(1).ToString();
                        }
                        else
                        {
                            result[i] = "NO_DB";
                        }
                    }
                    finally
                    {
                        reader.Close();
                        cmd.Dispose();
                        con.Close();
                        sqlConnectionBusy = false;
                    }
                }
                else
                {
                    result[i] = "";
                }
            }

            return result;
        }
        public static int CountingSup(string[] ackData)
        {
            int result = 0;

            for (int i = 0; i < 5; i++)
            {
                if (ackData[i + 4] != "")
                {
                    result += 1;
                }
            }

            return result;
        }
        public static int CountingPe(string[] peData)
        {
            int result = 0;

            for (int i = 0; i < 5; i++)
            {
                if (peData[i] != "")
                {
                    result += 1;
                }
            }

            return result;
        }

        //  Network Testing
        public static bool PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }

        //  Form Control
        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(System.Windows.Forms.Cursor.Position);
            }
        }
        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void Form1_Move(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                this.Hide();
                this.notifyIcon1.ShowBalloonTip(1000, "Auto Scanner Capture", "Application Started", ToolTipIcon.Info);
            }
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            notifyIcon1.Visible = false;
            notifyIcon1.Dispose();
            Environment.Exit(0);
        }
        #endregion

        #region PLC Portion
        //  Plc Set Programs
        public void Program_0_SetConenction()
        {
            if (PingHost(_PLC1IP))
            {
                _PLC1.NetworkAddress = _PLC1PeerSetting[0];
                _PLC1.NodeAddress = _PLC1PeerSetting[1];
                _PLC1.UnitAddress = _PLC1PeerSetting[2];
                _PLC1.ReceiveTimeLimit = _ReceiveTimeLimit;

                do
                {
                    if (CheckActive(_PLC1))
                    {
                        _PLC1.Update();

                        break;
                    }
                    else
                    {
                        wait(_DelayLoop);
                    }
                }
                while (true);
            }
        }
        public void Program_1_GetProductionData()
        {
            int CntOK = 0;

            int D0 = Fins_ReadMemoryWordInteger(_PLC1, 0);

            //  Change Model (Scan Next Kumban , Read Kumban Code)
            if (D0 == 1)
            {
                #region D0 = 1 Event
                int[] timeStart = new int[] { };

                //  Read Serial at D1 - D10
                _KumbanCode = Fins_ReadMemory(_PLC1, 100, 10);
                _KumbanCode = ASCII2String(_KumbanCode).Replace("\0", "");

                //  Read Time at D11 - D13 [ HH  mm  ss ]
                timeStart = Fins_ReadMemoryWordInteger_Array(_PLC1, 11, 3);

                timeStart[0] = int.Parse(timeStart[0].ToString("X").Replace("00", ""));
                timeStart[1] = int.Parse(timeStart[1].ToString("X"));
                timeStart[2] = int.Parse(timeStart[2].ToString("X"));

                //  Create Date Time Start
                DateTime dateNow = DateTime.Now;
                DateTime startDateTime = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, timeStart[0], timeStart[1], timeStart[2]);

                //  _ProductionPartNo
                //_ProductionPartNo = SearchProductionNoinDB(_KumbanCode);

                //  SELECT Production Code in DB
                if (_ProductionPartNo != "None")
                {
                    //  Insert New Actual Production in [ActualProductionData]
                    //InsertProductionOKtoDB(_ProductionPartNo, startDateTime);

                    //  Set D0 = 0
                    Fins_WriteMemoryWordInteger(_PLC1, 0, 2);
                }
                else
                {
                    //  Set D0 = 5 : Production code is missing !
                    Fins_WriteMemoryWordInteger(_PLC1, 0, 5);

                    string message = "Production Code ไม่พบใน Plan กรุณาตรวจสอบ Plan หรือหมายเลข Kumban ในระบบ";
                    string caption = "PLC Connection Message";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result;

                    // Displays the MessageBox.
                    result = MessageBox.Show(this, message, caption, buttons);

                    if (result == DialogResult.OK)
                        Fins_WriteMemoryWordInteger(_PLC1, 0, 10);
                }
                #endregion
            }

            //  Read Production Data
            else if (D0 == 2)
            {
                #region D0 = 2 Event

                //  Read Serial at D1 - D10
                _KumbanCode = Fins_ReadMemory(_PLC1, 100, 10);
                _KumbanCode = ASCII2String(_KumbanCode).Replace("\0", "");

                //  _ProductionPartNo
                //_ProductionPartNo = SearchProductionNoinDB(_KumbanCode);

                //  Read Prodcution Data : DM 14 Y182790003J
                CntOK = Fins_ReadMemoryWordInteger(_PLC1, 14);

                //  Update Cnt OK
                //UpdateProductionOKtoDB(_ProductionPartNo, _LineCode, CntOK);

                #endregion
            }

            //  
            else if (D0 == 3)
            {
                #region D0 = 3 Event
                int[] timeFinish = new int[] { };

                //  Read Serial at D1 -D10
                _KumbanCode = Fins_ReadMemory(_PLC1, 100, 10);
                _KumbanCode = ASCII2String(_KumbanCode).Replace("\0", "");

                if (_KumbanCode != "")
                {
                    //  _ProductionPartNo
                    //_ProductionPartNo = SearchProductionNoinDB(_KumbanCode);

                    //  Read Time at D15 -D17[HH  mm  ss]
                    timeFinish = Fins_ReadMemoryWordInteger_Array(_PLC1, 15, 3);

                    timeFinish[0] = int.Parse(timeFinish[0].ToString("X").Replace("00", ""));
                    timeFinish[1] = int.Parse(timeFinish[1].ToString("X"));
                    timeFinish[2] = int.Parse(timeFinish[2].ToString("X"));

                    //  Create Date Time Finish
                    DateTime dateNow = DateTime.Now;
                    DateTime finishDateTime = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, timeFinish[0], timeFinish[1], timeFinish[2]);

                    //  Update Ending Job
                    //UpdateProductionEndtoDB(_ProductionPartNo, _LineCode, finishDateTime);

                    //  Set D0 = 0
                    Fins_WriteMemoryWordInteger(_PLC1, 0, 0);

                }
                #endregion
            }
        }
        public static void Program_2_GetErrortimeEvent()
        {
            int D21 = Fins_ReadMemoryWordInteger(_PLC1, 20);
            int[] plcData = new int[] { };

            if ((D21 == 2) && (stateErr == false))
            {
                //  Stamp Log
                CreatAndApeendingLogFile("COM5", "RUN Program 2 : Rec D20 = 2");
                CreatAndApeendingLogFile("COM5", "RUN Program 2 : stateErr = true");
                int[] timeError = new int[] { };
                stateErr = true;
                //  Read Time at D21 - D23 [ HH  mm  ss ]
                timeError = Fins_ReadMemoryWordInteger_Array(_PLC1, 21, 3);
                timeError[0] = int.Parse(timeError[0].ToString("X").Replace("00", ""));
                timeError[1] = int.Parse(timeError[1].ToString("X"));
                timeError[2] = int.Parse(timeError[2].ToString("X"));
                //  Create Datetime from data of PLC <Year> <Month> <Day> <Hour> <Minute> <Second>
                DateTime dateNow = DateTime.Now;
                DateTime errorDateTime = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, timeError[0], timeError[1], timeError[2]);

                //  Stamp Error Datetime to DB
                InsertErrortimeEventtoDB(dateNow, _LinePLC);

                //  Set D20 = 0
                //Fins_WriteMemoryWordInteger(_PLC1, 20, 3);
            }
        }
        public static void Program_3_GetAcktimeEvent()
        {
            //  Check Ack event in this line
            //CreatAndApeendingLogFile("COM5", "CheckAckTimeEventHappened");
            bool stateAck = CheckAckTimeEventHappened();

            //  if have Ack from PDA
            if (stateAck)
            {
                CreatAndApeendingLogFile("COM5", "RUN Program 3 : stateErr = false");
                //  Set D21 = 3
                Fins_WriteMemoryWordInteger(_PLC1, 20, 3);
                //  Stamp Log
                CreatAndApeendingLogFile("COM5", "RUN Program 3 : Set D20 = 3");
                //  stateErr
                stateErr = false;
            }
        }
        //public static void Program_4_GetPEtimeEvent()
        //{
        //    //  Check Ack event in this line
        //    //bool statePE = CheckPETimeEventHappened();

        //    //  if have PE from PDA
        //    if (statePE)
        //    {
        //        CreatAndApeendingLogFile("COM5", "RUN Program 4 : stateErr = true");
        //        //  Set D21 = 3
        //        //  Fins_WriteMemoryWordInteger(_PLC1, 21, 3);
        //    }
        //}
        public static void Program_5_GetSuccessTimeEvent()
        {
            string[] ackData = new string[9];
            string[] peData = new string[5];
            DateTime finTime = new DateTime();

            try
            {
                //  Check Ack event in this line
                bool stateFinish = CheckFinTimeEventHappened();

                //  if have Suscess from PDA
                if (stateFinish)
                {
                    //  Stamp Log
                    //CreatAndApeendingLogFile("COM5", "RUN Program 5 : RUN Program 5 : stateErr = true");

                    //  SELECT ACK DB
                    //ackData = SelectkAcktimeEventFromPDA();

                    //  SELECT PE DB
                    //peData = SelectkPetimeEventFromPDA();

                    //  SELECT FIN DB
                    //finTime = SelectkFintimeEventFromPDA();

                    //  INSERT [ActualMachineLoss]
                    //InsertActualMachineLoss(ackData, peData, finTime);

                    //  DELETE [TempMC_ErrorTime] , [TempMC_AckTime] , [TempMC_PE] , [TempMC_FinishTime]
                    //DateTime errorTime = Convert.ToDateTime(ackData[1]);
                    //string mcCode = ackData[0];

                    //DeleteTempMC_ErrorTime(_LinePLC, errorTime);
                    //DeleteTempMC_AckTime(_LinePLC, mcCode, errorTime);
                    //DeleteTempMC_PE(_LinePLC, mcCode, errorTime);
                    //DeleteTempMC_FinishTime(_LinePLC, mcCode, errorTime);

                    //  Set D20 = 1
                    Fins_WriteMemoryWordInteger(_PLC1, 20, 1);
                    stateErr = false;

                    //  Stamp Log
                    //CreatAndApeendingLogFile("COM5", "RUN Program 5 : Set D20 = 1");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "_" + ex.StackTrace, "Program_5_GetSuccessTimeEvent");
            }

        }
        public static void Program_6_GetAcktimeNG()
        {
            bool hasAckNG = CheckAcktimeNGEventHappened();
            //  if have PE from PDA
            if (hasAckNG)
            {
                //  Set D20 = 3
                Fins_WriteMemoryWordInteger(_PLC1, 20, 1);
            }
        }
        public static void Program_7_GetRepairtimeNG()
        {
            CheckRepairtimeNGEventHappened();
        }

        //  PLC Sub Programs
        public bool CheckActive(SysmacCJ PLC)
        {
            bool result;
            try
            {
                PLC.Active = true;

                if (PLC.Active)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch
            {
                wait(_DelayLoop);
                result = false;
            }
            return result;
        }

        public string Fins_ReadMemory(SysmacCJ PLC, long DM, long Cnt)
        {
            string result = "";
            do
            {
                try
                {
                    result = PLC.ReadMemory(SysmacCSBase.MemoryTypes.DM, DM, Cnt, true).ToString();
                    break;
                }
                catch
                {
                    wait(_DelayLoop);
                }
            } while (true);
            return result;
        }

        public static int Fins_ReadMemoryWordInteger(SysmacCJ PLC, long DM)
        {
            Int32 result = 0;
            do
            {
                try
                {
                    result = PLC.ReadMemoryWordInteger(SysmacCSBase.MemoryTypes.DM, DM, SysmacC.DataTypes.BIN);
                    break;
                }
                catch
                {
                    wait(_DelayLoop);
                }
            } while (true);
            return result;
        }
        public static int[] Fins_ReadMemoryWordInteger_Array(SysmacCJ PLC, long DM, long Cnt)
        {
            int[] result = new int[] { };
            do
            {
                try
                {
                    result = PLC.ReadMemoryWordInteger(SysmacCSBase.MemoryTypes.DM, DM, Cnt, SysmacC.DataTypes.BIN);
                    break;
                }
                catch
                {
                    wait(_DelayLoop);
                }
            } while (true);
            return result;
        }
        public static void Fins_WriteMemoryWordInteger(SysmacCJ PLC, long DM, int Value)
        {
            do
            {
                try
                {
                    PLC.WriteMemoryWordInteger(SysmacCSBase.MemoryTypes.DM, DM, Value, SysmacC.DataTypes.BIN);
                    break;
                }
                catch
                {
                    wait(_DelayLoop);
                }
            } while (true);
        }

        //  Fin Commu.
        public void SetFinAddr(short[] PeerSetup, long _ReceiveTimeLimit)
        {
            do
            {
                try
                {
                    _PLC1.NetworkAddress = PeerSetup[0];
                    _PLC1.NodeAddress = PeerSetup[0];
                    _PLC1.UnitAddress = PeerSetup[0];
                    _PLC1.ReceiveTimeLimit = _ReceiveTimeLimit;
                    break;
                }
                catch
                {
                    wait(_DelayLoop);
                }
            } while (true);
        }
        #endregion

        //  Log System
        private static void CreatAndApeendingLogFile(string COM, string TextMessage)
        {
            string strFolder = _Path;
            string strFiles = strFolder + "Log_" + _LineCode + "_" + _Shift + "_" + COM + "_" + DateTime.Today.ToString("ddMMyyyy") + ".txt";

            StreamWriter sw;
            FileStream fs;

            if (!Directory.Exists(strFolder))
                Directory.CreateDirectory(strFolder);

            if (!File.Exists(strFiles))
            {
                try
                {
                    fs = File.Create(strFiles);
                    fs.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "CreatAndApeendingLogFile");
                }
            }

            sw = File.AppendText(strFiles);
            sw.WriteLine(DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss") + " ---- " + TextMessage);
            sw.Close();
        }
    }
}
