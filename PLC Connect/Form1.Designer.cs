﻿namespace PLC_Connect
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.timerMainLoop = new System.Windows.Forms.Timer(this.components);
            this.serialPortBegin = new System.IO.Ports.SerialPort(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.comboBoxPN_Ship = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxPN_LineName = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonPN_LineSetup = new System.Windows.Forms.Button();
            this.labelSetupFinish = new System.Windows.Forms.Label();
            this.buttonLM_Done = new System.Windows.Forms.Button();
            this.contextMenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerMainLoop
            // 
            this.timerMainLoop.Interval = 1000;
            // 
            // serialPortBegin
            // 
            this.serialPortBegin.PortName = "COM5";
            this.serialPortBegin.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPortBegin_DataReceived);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(614, 92);
            this.label1.TabIndex = 0;
            this.label1.Text = "Automatic Production Counting (SMT)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(104, 48);
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.showToolStripMenuItem.Text = "Show";
            this.showToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseClick);
            // 
            // comboBoxPN_Ship
            // 
            this.comboBoxPN_Ship.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboBoxPN_Ship.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.comboBoxPN_Ship.FormattingEnabled = true;
            this.comboBoxPN_Ship.Items.AddRange(new object[] {
            "Shift A",
            "Shift B",
            "Shift C"});
            this.comboBoxPN_Ship.Location = new System.Drawing.Point(316, 168);
            this.comboBoxPN_Ship.Name = "comboBoxPN_Ship";
            this.comboBoxPN_Ship.Size = new System.Drawing.Size(233, 33);
            this.comboBoxPN_Ship.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(311, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 26);
            this.label2.TabIndex = 17;
            this.label2.Text = "Shift Time : ";
            // 
            // comboBoxPN_LineName
            // 
            this.comboBoxPN_LineName.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboBoxPN_LineName.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.comboBoxPN_LineName.FormattingEnabled = true;
            this.comboBoxPN_LineName.Items.AddRange(new object[] {
            "K-7901",
            "K-7902",
            "K-7903",
            "K-7904"});
            this.comboBoxPN_LineName.Location = new System.Drawing.Point(51, 168);
            this.comboBoxPN_LineName.Name = "comboBoxPN_LineName";
            this.comboBoxPN_LineName.Size = new System.Drawing.Size(233, 33);
            this.comboBoxPN_LineName.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(46, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 26);
            this.label3.TabIndex = 18;
            this.label3.Text = "Line Code : ";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(614, 92);
            this.panel1.TabIndex = 20;
            // 
            // buttonPN_LineSetup
            // 
            this.buttonPN_LineSetup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(150)))));
            this.buttonPN_LineSetup.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.buttonPN_LineSetup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.buttonPN_LineSetup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPN_LineSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.buttonPN_LineSetup.ForeColor = System.Drawing.Color.White;
            this.buttonPN_LineSetup.Location = new System.Drawing.Point(51, 223);
            this.buttonPN_LineSetup.Name = "buttonPN_LineSetup";
            this.buttonPN_LineSetup.Size = new System.Drawing.Size(233, 56);
            this.buttonPN_LineSetup.TabIndex = 30;
            this.buttonPN_LineSetup.Text = "SET";
            this.buttonPN_LineSetup.UseVisualStyleBackColor = false;
            this.buttonPN_LineSetup.Click += new System.EventHandler(this.buttonPN_LineSetup_Click);
            // 
            // labelSetupFinish
            // 
            this.labelSetupFinish.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labelSetupFinish.ForeColor = System.Drawing.Color.LimeGreen;
            this.labelSetupFinish.Location = new System.Drawing.Point(181, 298);
            this.labelSetupFinish.Name = "labelSetupFinish";
            this.labelSetupFinish.Size = new System.Drawing.Size(245, 26);
            this.labelSetupFinish.TabIndex = 31;
            this.labelSetupFinish.Text = "การตั้งค่าการทำงานสำเร็จ !!";
            this.labelSetupFinish.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSetupFinish.Visible = false;
            // 
            // buttonLM_Done
            // 
            this.buttonLM_Done.BackColor = System.Drawing.Color.SeaGreen;
            this.buttonLM_Done.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.buttonLM_Done.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumSeaGreen;
            this.buttonLM_Done.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLM_Done.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.buttonLM_Done.ForeColor = System.Drawing.Color.White;
            this.buttonLM_Done.Location = new System.Drawing.Point(316, 223);
            this.buttonLM_Done.Name = "buttonLM_Done";
            this.buttonLM_Done.Size = new System.Drawing.Size(233, 56);
            this.buttonLM_Done.TabIndex = 69;
            this.buttonLM_Done.Text = "RESET";
            this.buttonLM_Done.UseVisualStyleBackColor = false;
            this.buttonLM_Done.Click += new System.EventHandler(this.buttonLM_Done_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(614, 342);
            this.Controls.Add(this.buttonLM_Done);
            this.Controls.Add(this.labelSetupFinish);
            this.Controls.Add(this.buttonPN_LineSetup);
            this.Controls.Add(this.comboBoxPN_LineName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBoxPN_Ship);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Serial Connection";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Move += new System.EventHandler(this.Form1_Move);
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timerMainLoop;
        private System.IO.Ports.SerialPort serialPortBegin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ComboBox comboBoxPN_Ship;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxPN_LineName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonPN_LineSetup;
        private System.Windows.Forms.Label labelSetupFinish;
        private System.Windows.Forms.Button buttonLM_Done;
    }
}

